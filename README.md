
# ArmarX Setup Tool

This tool allows you to install and configure ArmarX along with its various optional dependencies.

**Note:** This project is being replaced by: https://gitlab.com/ArmarX/meta/setup


# Usage

If you have not already added a SSH key to your GitLab account, please follow the guidelines here: [Create and add your SSH key pair](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)

Then, clone this repository:
```bash
$> git clone git@gitlab.com:ArmarX/armarx-setup.git
cd armarx-setup
```

You will find, among others, the directories ```profiles``` and ```setup-18-bits``` as well as the executable script `setup-18`.
`setup-18` is the main script we will run to pull the necessary source code and libraries.
But before, we have to do some configuration.

## Configure your profile

This tool allows creating profiles which specify which packages to pull and how to do it.
In `profiles/`, you will find some profile templates, such as `minimal` and `default`.
You can edit and use them directly, or you can create your own profile based on one of them.
We will create a new profile `myusername` based on `minimal`:

- Copy `profiles/minimal` to `profiles/myusername`.
- Open the new file `profiles/myusername`.

In the profile, you can set several variables to customize your setup and setup process.
Fill in the empty values and update the others as needed. Keep in mind to have spaces around `=`.
Some important variables are:

| Variable | Description |
| ------ | ------ |
| `staff` | Whether you are H2T staff (if false, some non-public repositories are not fetched.) |
| `git_user, git_mail` | Your git username and email address. | 
| `keyfile` |  Path to your private SSH key (for git access via SSH). |
| `skip install, skip sudo` |  If true, installation commands and commands needing sudo rights are skipped. (Useful for shared lab PCs.) |
| `target dir` |  If true, installation commands and commands needing sudo rights are skipped. (Useful for shared lab PCs.) |

At the bottom, you will find a list of lines in the format:
```bash
...
skip_simox=false
skip_simox_cgal=true
...
```
These variables determine which software packages will be fetched (or more precisely, which packages won't be fetched).
Each `skip_<groupname>=true/false` refers to a "group", which can consist of a single or multiple repositories.
Set the `skip_<groupname>=...` lines of the groups you want to `false` (so they are not skipped), and set the others to true.
> What is part of each group is specified in `setup-18-bits/zzz_packages`. 
> There is a large table specifying the repository URLs, branches and other information. 
> Check this file to see what the `<groupname>` in the `skip_<groupname>=...` line means.

When you are done editing your config, we have to tell the script which profile to load:
- Open `setup-18-bits/000_user_config`.
  - Set `profile_to_load="myusername"`. (Where `myusername` is the name of the profile. Mind the double quotes around `myusername`)

## Run the setup script

Now you can run the install script.
```bash
$> ./setup-18
```

### Outdated config template

You may receive the following message:

```
$> ./setup-18
[...]
Checking your config for errors (profile: myusername)

ERROR: Your config is broken (profile: myusername)

Some groups were missing a configuration whether they sould be skipped!
Set the variable skip_<groupname> either to true or to false
This are the groups:
    blaze
    [...]
    dlib
There is a template in '/tmp/filehgsI1q'

ERROR: Your config is broken (profile: myusername)

something in your config is wrong!

```
This means that the profile template you used was probably outdated and missed
some group entries. To add them, open the specified file (`/tmp/filehgsI1q` in this example; **see the console output for your filename**).
This file will contain your skip settings along with the missing lines, which will have the format:
```bash
skip_blaze= #<<<<<<<<<<<<
```
Now, simply replace the `skip_*=...` lines in your profile with the content of this file.
Then, replace the ` #<<<<<<<<<<<<` with true or false depending on your needs. (Again, take care not to have a space after `=`.)
When you are finished, save your profile and rerun `./setup-18`.

  
### Running the script

When the profile is valid, the script will start cloning all necessary repositories (in parallel unless `para_cap` was set to false).
When asked for, enter your Redmine credentials (username, password).
Once all cloning processes are finished, the script will start building the pulled packages.
You can `Ctrl+C` at any point to stop the script. 
When restarting setup script (optionally with an updated profile), already pulled packages will not be pulled again (unless specified otherwise in your profile).

If it looks like cloning gets stuck, it may help to disable "parallel clone and pull" by setting `para_cap=false` in your profile.
Sometimes, getting stuck is caused by errors during cloning, e.g. when you are lacking access rights for a repository.
In this case, update your profile to skip the according group and rerun the setup script.

When building fails for some reason, you can investigate and fix the error and restart the script. 
To skip cloning and pulling completely, set `skip_clone_and_pull=true` in your profile.
You can also force cleaning builds by setting `clean_build=true`.

