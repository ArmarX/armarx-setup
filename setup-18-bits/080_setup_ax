#!/bin/bash -xe
####################################################################
#cmake ArmarXCore
####################################################################
cd_clean_cmk "$ArmarX_DIR/ArmarXCore"

####################################################################
#profiles
####################################################################
pr=
[ -e "$HOME/.armarx/default.cfg" ] && pr="$(ax profile | grep -vE "^ *(|>.*)$" | grep '^  \* ' | cut -c5-)"

ax switch localhost
ax switch armar6a-0 --ice-port=4061 --ice-host=10.6.2.100 --mongo-port=27017 --mongo-host=10.6.2.100
ax switch armar6a-1 --ice-port=4061 --ice-host=10.6.2.101 --mongo-port=27017 --mongo-host=10.6.2.101
ax switch armar6a-2 --ice-port=4061 --ice-host=10.6.2.102 --mongo-port=27017 --mongo-host=10.6.2.102
ax switch armar6a-3 --ice-port=4061 --ice-host=10.6.2.103 --mongo-port=27017 --mongo-host=10.6.2.103

ax switch default
####################################################################
#build armarx
####################################################################
cd $ArmarX_DIR
all_cmk="$ArmarX_DIR/all/CMakeLists.txt"
if [ -e "$all_cmk" ]
then
    if $clean_build
    then
        axe all [ ]             | \
            cut -d " " -f 3     | \
            grep -v ArmarXCore  | \
            grep -v ArmarXGui   | \
            grep -v all         | \
            while read p
            do
                rm -rf build/*
            done
    fi
    ! $reset_all_package || rm -rf "$ArmarX_DIR/all"
fi

[ -e "$all_cmk" ] || axp init all
add_to_all_and_additional_pkgs()
{
    ax-add-additionalpackages $1
    if ! grep -q "$1" "$all_cmk"
    then
        marker="add_subdirectory(etc)"
        req="depends_on_armarx_package($1 \"OPTIONAL\")"
        sed -i "s/$marker/$req\n$marker/g" "$ArmarX_DIR/all/CMakeLists.txt" || exit 1
        echo "-- added $1 as dep to all/CMakeLists.txt"
    else
        echo "-- package $1 was already added as dep to all/CMakeLists.txt"
    fi
}
####################################################################
#build all package
unsetx
while read line
do
    cd "$ArmarX_DIR"
    package_line_parse "$line"
    dir="$ArmarX_DIR/$package_target_dir"
    lvl="$package_level"
    build="$package_build"
    flags="$package_flags"


    $package_skip && continue
    [ "$lvl"   == "ax" ] || continue
    [ "$build" == "ax" ] || continue

    if [ -z "$flags" ]
    then
	    echo "Project in $dir"
        project_name="$(ax-project-name "$dir")"
        if [ -z "$project_name" ]
        then
            RED echo "ERROR the directory '$dir' does not contain an ArmarX project"
            exit 1
        fi
        add_to_all_and_additional_pkgs "$project_name"
    elif [ "$flags" == "search" ]
    then
        echo "Project in subdirs of  $dir"
        cd "$dir"
        cmake-find-toplevel-lists | while read tlcml
        do
            project_name="$(ax-project-name "$tlcml")"
            [ -z "$project_name" ] || add_to_all_and_additional_pkgs "$project_name"
        done
    else
        RED echo "[$dir] has build type ax but unknown flags"
        RED echo "$line"
        exit 1
    fi
done <<< "$(packages)"
restorex

####################################################################
#add dependencies
$skip_simox         || ax-add-dependency ArmarXCore        Simox
$skip_mmm           || ax-add-dependency ArmarXCore        MMMCore
$skip_mmm           || ax-add-dependency ArmarXCore        MMMTools
$skip_dmp           || ax-add-dependency ArmarXCore        DMP
$skip_affordance    || ax-add-dependency ArmarXCore        AffordanceKit
$skip_simox_cgal    || ax-add-dependency ArmarXCore        simox-cgal

$skip_hokuyo        || ax-add-dependency RobotAPI          HokuyoLaserScannerDriver

$skip_humloc        || ax-add-dependency Armar4RT          Bipedal

$skip_ivt           || ax-add-dependency VisionX           IVT
$skip_ivtrec        || ax-add-dependency VisionX           IVTRecognition
$skip_ivta3         || ax-add-dependency VisionX           IVTArmar3
$skip_semrel        || ax-add-dependency VisionX           SemanticObjectRelations

$skip_hapticexp     || ax-add-dependency HapticExploration HapticExplorationLibrary

$skip_semrel        || ax-add-dependency SemantiX          SemanticObjectRelations

####################################################################
#build
cd "$ArmarX_DIR"
retry $attempts axb -c all || axb-ignore-failure all
retry $attempts axb -c all


####################################################################
#$skip_armarxqp                                             || \
#    {                                                         \
#        add_to_all ArmarXQP                                && \
#        cd_clean_build "$ArmarX_DIR/qp-control/ArmarXQP" ;    \
#    }                                                      || \
#    $allow_failure_armarxqp

####################################################################
#memory
####################################################################
cd $ArmarX_DIR
if retry $attempts ax memory start
then
    for import_db in "$ArmarX_DIR/ArmarXDB/data/ArmarXDB/dbexport/memdb"                    \
                     "$ArmarX_DIR/Armar6Skills/data/Armar6Skills/dbexport/SecondHandsDB"    \
                     "$ArmarX_DIR/Armar6Skills/data/Armar6Skills/dbexport/CeBITdb"          \
                     "$ArmarX_DIR/ARCHES/data/ARCHES/dbexport/arches"                       \
                     "$ArmarX_DIR/ROBDEKON_Sim/data/ROBDEKON_Sim/dbexport"
    do
        [ -d "$import_db" ] || continue
        retry $attempts "$ArmarX_DIR/MemoryX/build/bin/mongoimport.sh" "$import_db" || \
            echo "Failed to import '$import_db'"
    done
    retry $attempts ax memory stop || true
else
    echo "FAILED TO START ARMARX MEMORY"
fi

[ -z "$pr" ] || ax switch "$pr"
####################################################################
#RobotComponents
####################################################################
$skip_ax || ENV_ONLY=1 "$ArmarX_DIR/RobotComponents/data/RobotComponents/python/PoseBasedActionRecognizer/startPoseBasedActionRecognizer.sh"
####################################################################
#armar6 speech
####################################################################
$skip_armar6 || "$ArmarX_DIR/Armar6Speech/etc/scripts/setup-envs"

