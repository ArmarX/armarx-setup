#!/bin/bash -xe
####################################################################
#load profile
####################################################################
loaded_profile=

cd "$profile_dir"

if [ -z "$profile_to_load" ]
then
    check_and_load_profile() # regex_whoami regex_hostname
    {
        if  [ -z "$loaded_profile"     ] &&
            [[ "$(whoami)"      =~ $1 ]] &&
            [[ "$(hostname -s)" =~ $2 ]]
        then
            loaded_profile="$3"
            source "$3"
        fi
    }

    check_and_load_profile '.*'                     'i61ar.*'          arches-board
    check_and_load_profile '^(raphael|grimm)[0-9]?' '.*'               raphael
    check_and_load_profile '^armar-(demo|user)'     '(armar6a|a6ac).*' armar6
    check_and_load_profile '^rp[0-9]+'              '.*'               minimal
else
    echo "you set profile_to_load to '$profile_to_load'"
    echo "no profile  detection was executed!"
    echo "set profile_to_load to an epty string to use autodetection!"
fi

if [ -z "$loaded_profile" ]
then
    [ -z "$profile_to_load" ] && profile_to_load=default
    loaded_profile="$profile_to_load"
    source "$profile_to_load"
fi

cd -
####################################################################
#vars (urls)
####################################################################
readonly wiki="https://i61wiki.itec.uka.de/git"

readonly ghub="https://github.com"
readonly ghub_rc="$ghub/roboception"
readonly ghub_sh="$ghub/SecondHands"

readonly bbuck="https://bitbucket.org"

readonly bbhdf="https://bitbucket.hdfgroup.org/scm"

readonly glab="https://gitlab.com"
readonly glab_ax="$glab/ArmarX"
readonly glab_h2t="$glab/h2t"
readonly glab_mmm="$glab/mastermotormap"
readonly glab_h2t_res="$glab/h2t/ResearchProjects"

readonly mujoco_dl_url="https://www.roboti.us/download"
####################################################################
#vars (dirs)
####################################################################
readonly rel_target_dir="$(realpath --relative-to="$HOME" "$target_dir")"
readonly flycapture_dir="flycapture2-2.13.3.31-amd64"
readonly aruco_dir="aruco-3.0.12"
readonly openpose_dir="openpose"
ArmarX_DIR="$target_dir"
####################################################################
#vars (files)
####################################################################
readonly armarxrc="$ArmarX_DIR/.armarxrc"
readonly bashrc="$HOME/.bashrc"
readonly bash_profile="$HOME/.bash_profile"
readonly profile="$HOME/.profile"
readonly armarxini="$HOME/.armarx/armarx.ini"
####################################################################
#vars (shorthands git)
####################################################################
readonly track_origin="--track remotes/origin"
readonly track_i37="$track_origin/Ice3.7"
readonly track_u18="$track_origin/Ubuntu18"
####################################################################
#vars (shorthands)
####################################################################
readonly rc=roboception
readonly cmu=CMU-Perceptual-Computing-Lab
readonly sor=semantic-object-relations
readonly humloc=humanoidlocomotion
readonly sh_daol=dynamic_obstacle_avoidance_linear
####################################################################
#package vars (defined here and used in 06 and 07
####################################################################
readonly opose_cudnn_ver="5.1"
readonly opose_cudnn_tar_body="cudnn-8.0-linux-x64-v$opose_cudnn_ver"
readonly opose_cudnn_dir="$ArmarX_DIR/openpose/3rdparty/$opose_cudnn_tar_body"
####################################################################
#derived settings
####################################################################
one_install_dir_set=false
[ -z "$one_install_dir" ] || one_install_dir_set=true
readonly one_install_dir_set
####################################################################
#global helper fnc
####################################################################
#util
unsetx()
{
    _xflags=$-
    set +x
}
restorex()
{
    set -$_xflags
}
clear_screen()
{
    unsetx
    i=0
    while [ $i -lt 100 ]
    do
        i=$(($i+1))
        echo
    done
    restorex
}

bluecho()
{
    unsetx
    echo -en '\033[1;34m'
    echo -e "$@"
    echo -en '\033[0m'
    restorex
}
retry()
{
    set +e
    retr="$(which retry)"
    set -e
    if [ -z "$retr" ]
    then
        n=0
        eval "${@:2}"
        returned=$?
        while ! [ $returned -eq 0 ]
        do
            bluecho "${@:2} -> $returned"
            n=$(($n + 1))
            if ! [ $n -le $1 ]
            then
                break
            fi
            bluecho "Retrying ($n/$1): ${@:2}"
            eval "${@:2}"
            returned=$?
        done
    else
        $retr $@
        returned=$?
    fi
    return $returned
}
add_if_not_in_file()
{
    grep -Fq "${@:2}" "$1" || echo "${@:2}" >> "$1"
}
eval_at_exit=
store_eval_at_exit()
{
    unsetx
    eval_at_exit="$eval_at_exit $@;"
    restorex
}
packages()
{
    cat "$bits_dir/zzz_packages" | grep -v '^#' | grep -v '^[ \t]*$' | sort
}
package_line_parse()
{
    unsetx
    raw_package_level="$(     echo "$1" | cut -d '|' -f 1 | xargs)"
    raw_package_type="$(      echo "$1" | cut -d '|' -f 2 | xargs)"
    raw_package_group="$(     echo "$1" | cut -d '|' -f 3 | xargs)"
    raw_package_url="$(       echo "$1" | cut -d '|' -f 4 | xargs)"
    raw_package_target_dir="$(echo "$1" | cut -d '|' -f 5 | xargs)"
    raw_package_checkout="$(  echo "$1" | cut -d '|' -f 6 | xargs)"
    raw_package_build="$(     echo "$1" | cut -d '|' -f 7 | xargs)"
    raw_package_flags="$(     echo "$1" | cut -d '|' -f 8 | xargs)"

    package_level="$(     eval "echo \"$raw_package_level\"")"
    package_type="$(      eval "echo \"$raw_package_type\"")"
    package_group="$(     eval "echo \"$raw_package_group\"")"
    package_url="$(       eval "echo \"$raw_package_url\"")"
    package_target_dir="$(eval "echo \"$raw_package_target_dir\"")"
    package_checkout="$(  eval "echo \"$raw_package_checkout\"")"
    package_build="$(     eval "echo \"$raw_package_build\"")"
    package_flags="$(     eval "echo \"$raw_package_flags\"")"
    restorex
    package_skip_var="skip_$package_group"
    package_skip="${!package_skip_var}"
}
####################################################################
#install (SEE 04)	
set +x #reduce spam
pkgs_i="$(apt list --installed | cut -d/ -f1)"
set -x
install_apt()
{
    if $skip_install
    then
        unsetx
            for p in $@
            do
                if ! echo "$pkgs_i" | grep -q ^$p$
                then
                    bluecho "NOT INSTALLED: $p"
                    if [ -z "$install_apt_not_installed_marker_written" ]
                    then
                        install_apt_not_installed_marker_written="1"
                        echo "------------$(date --rfc-3339=ns)------------" >> NOT_INSTALLED
                    fi
                    echo "$p" >> NOT_INSTALLED
                fi
            done
        restorex
    else
        sudo apt-get install -y $@
    fi
}
install_pip()
{
    $skip_install || sudo pip install $@ --upgrade
}

####################################################################
#keep credentials alive (SEE 03)
keep_alive()
{
    eval "$@"
    bash -c "while true; do $1; sleep 10; done;" &
    kapids="$kapids $!"
    trap '[ ! -z "$kapids" ] && kill -9 $kapids; eval "$eval_at_exit"' EXIT HUP TERM INT
}
####################################################################
#cloning (SEE 05)
repo_n=0
repo_n_current=0
cap_i() #IMPL# clone and pull (git): PARAMS: url targetfolder branch
{
    $skip_clone_and_pull && return
    [ ! -e $2/.git ] && retry $attempts git clone $1 $2
    cd "$2"
    retry $attempts "sleep 0.1; git lfs install" || true
    [ "$4" != "--submod" ] || retry $attempts git submodule update --init
    retry $attempts git fetch -p --all
    [ $# -gt 2 ] && git branch | grep -q "* master" && git checkout $3
    git pull || true
    git lfs pull || true
    cd -
    repo_n_current=$((repo_n_current+1))
    if $para_cap
        then bluecho "DONE pulling $1"
        else bluecho "DONE pulling $1 ($repo_n_current / $repo_n)"
    fi
}
cap() #DEPLOY# clone and pull (git) might be async: PARAMS:  url targetfolder branch [--submod]
{
    if $para_cap
    then
        cap_i "$1" "$2" "$3" "$4" &
        pid=$!
        cappids="$cappids $pid"
        eval "process_$pid='cap $@'"
    else
        cap_i "$1" "$2" "$3"
    fi
}
cap_hg_i() #IMPL# clone and pull (hg): PARAMS: url targetfolder branch
{
    $skip_clone_and_pull && return
    [ ! -e $2 ] && retry $attempts hg clone $1 $2
    cd $2
    [ ! -z "${@:3}" ] && hg branch | grep -q "default" && hg update ${@:3}
    hg pull -u || true
    cd -
    repo_n_current=$((repo_n_current+1))
    if $para_cap
        then bluecho "DONE pulling $1"
        else bluecho "DONE pulling $1 ($repo_n_current / $repo_n)"
    fi
}
cap_hg() #DEPLOY# clone and pull (hg) might be async: PARAMS:  url targetfolder branch
{
    if $para_cap
    then
        cap_hg_i "$1" "$2" "$3" &
        pid=$!
        cappids="$cappids $pid"
        eval "process_$pid='cap_hg $@'"
    else
        cap_hg_i "$1" "$2" "$3"
    fi
}

cp_uzip_i() #IMPL# copy and unzip: PARAMS: url targetfolder
{
    $skip_clone_and_pull && return
    [ -e "$ArmarX_DIR/$2" ] && return

    # the file could be a prefix!
    tmp_file="$(readlink -f "$(find "$(dirname "$1")" -type f -path "$1")")"

    parentdir="$(dirname "$ArmarX_DIR/$2")"
    mkcd "$parentdir"
    if [[ "$tmp_file" =~ ^.*.tar.gz$ ]] || [[ "$tmp_file" =~ ^.*.tgz$ ]]
        then tar xzf "$tmp_file"
        else 7z  x   "$tmp_file"
    fi
    cd -
    repo_n_current=$((repo_n_current+1))
    if $para_cap
        then bluecho "DONE unziping $1"
        else bluecho "DONE unziping $1 ($repo_n_current / $repo_n)"
    fi
}
cp_uzip() #DEPLOY# copy and unzip might be async: PARAMS:  url targetfolder
{
    if $para_cap
    then
        cp_uzip_i "$1" "$2" &
        pid=$!
        cappids="$cappids $pid"
        eval "process_$pid='cp_uzip $@'"
    else
        cp_uzip_i "$1" "$2"
    fi
}

wget_uzip_i() #IMPL# wget and unzip: PARAMS: url targetfolder
{
    $skip_clone_and_pull && return
    [ -e "$ArmarX_DIR/$2" ] && return

    tmp_dir="$ArmarX_DIR/.tmp/$RANDOM$RANDOM$RANDOM"
    mkcd "$tmp_dir"
    wget "$1"
    tmp_file="$(readlink -f "$(find "$tmp_dir" -type f)")"

    cd -
    parentdir="$(dirname "$ArmarX_DIR/$2")"
    mkcd "$parentdir"
    if [[ "$tmp_file" =~ ^.*.tar.gz$ ]] || [[ "$tmp_file" =~ ^.*.tgz$ ]]
        then tar xzf "$tmp_file"
        else 7z  x   "$tmp_file"
    fi
    cd -
    repo_n_current=$((repo_n_current+1))
    if $para_cap
        then bluecho "DONE downloading and unziping $1"
        else bluecho "DONE downloading and unziping $1 ($repo_n_current / $repo_n)"
    fi
    rm -rf "$tmp_dir"
}
wget_uzip() #DEPLOY# wget and unzip might be async: PARAMS:  url targetfolder
{
    if $para_cap
    then
        wget_uzip_i "$1" "$2" &
        pid=$!
        cappids="$cappids $pid"
        eval "process_$pid='wget_uzip $@'"
    else
        wget_uzip_i "$1" "$2"
    fi
}

wait_for_para_cap() #waits for all parallel clone processes and prints nice output
{
    unsetx

    regex="^(_$(echo "$cappids" | tr ' ' '|'))$"
    while true
    do
        jobs > /dev/null # this seems to be required to update the jobs list
        running="$(jobs -p | grep -E "$regex" | xargs)"
        if [ -z "$running" ]
        then
           bluecho "All cap processes are done!"
           break
        fi

        bluecho "waiting for:"
        for pid in $running
        do
            varname="process_$pid"
            bluecho "---- $pid: ${!varname}"
        done

        sleep 1
    done
    errors=""
    ok=""
    bluecho "collecting results"
    set +e #we manually kill after the loop
    for p in $cappids
    do
        # wait for single pids since failure is only reported for the last pid
        wait $p
        r=$?
        varname="process_$p"
        if [ $r -eq 0 ]
        then
            ok="$ok$r : ${!varname}\n"
        else
            errors="$errors$r : ${!varname}\n"
        fi
    done
    set -e
    bluecho -e "$ok\n$errors"
    if ! [ -z "$errors" ]
    then
        bluecho "SOME cap had non zero return code"
        exit 1
    fi
    restorex
    cappids=""
}
retry-wget()
{
    retry $attempts wget $@
}
####################################################################
#add to files (SEE 06)
add_if_not_in_bashrc()
{
    add_if_not_in_file "$bashrc" "$@"
    eval "$@"
}

add_if_not_in_axini()
{
    mkdir -p "$(dirname "$armarxini")"
    touch "$armarxini"
    add_if_not_in_file $armarxini "$@"
}

add_to_armarxrc()
{
    mkdir -p "$(dirname "$armarxrc")"
    add_if_not_in_file "$armarxrc" "$@"
    eval "$@"
}

add_export_to_armarxrc()
{
    [ $# -eq 2 ] || return 1
    add_to_armarxrc "export $1=\"$2\""
}

####################################################################
#building (SEE 07)
clean()
{
    $clean_build && retry $attempts rm -rf ../build/* || true ;
}
cmk()
{
    retry $attempts cmake $@ || $continue_on_failed_build || return 1 ;
}
mk()
{
    retry $attempts make  $@ || $continue_on_failed_build || return 1 ;
}
# $1 = path to build, $2 = cmake flags, $3 = cmds to exec before running cmake
cd_clean_cmk()
{
    mkcdir "$1/build"
    eval "$3"
    clean
    cmk $2 ..
}
# $1 = path to build, $2 = cmake flags, $3 = cmds to exec before running cmake
cd_clean_build()
{
    cd_clean_cmk "$1" "$2" "$3"
    mk
}
cd_clean_build_install()
{
    cd_clean_build "$1" "$2" "$3"
    retry $attempts make install
}
readonly ccbi=cd_clean_build_install
idir() #select correct installdir ($one_install_dir if set, otherwise $@)
{
    if [ -z "$one_install_dir" ]
        then echo "$@"
        else echo "$one_install_dir"
    fi
}
ipre() #same as idir but adds the cmake install prefix bloat
{
    echo "-DCMAKE_INSTALL_PREFIX='$(idir "$@")'"
}
