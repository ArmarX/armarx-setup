#!/bin/bash -xe
set +x
failure=false

################################################################################
# check for errors
################################################################################
bluecho "Checking your config for errors (profile: $loaded_profile)"

error_flags=
while read line
do
    [ -z "${!line}" ] || continue
    error_flags="$error_flags$line\n"
    failure=true
done <<< "$(cat "$bits_dir/zzz_config_flags" | grep -v '^#' | grep -v '^[ \t]*$' | sort)"


error_lines=
error_skip=
error_level=
error_type=
error_group=
error_target_dir=
error_build=

error_skip_template_file="$(tempfile)"
error_skip_template_file_sorted="$(tempfile)"

while read line
do
    package_line_parse "$line"
    set +x
    line_error=false
    if [ "$package_skip" != "true"  ] &&  [ "$package_skip" != "false" ]
    then
        failure=true
        line_error=true
        $line_error || error_lines="$error_lines$line\n"
        error_skip="$error_skip$package_group\n"
        echo "skip_$package_group= #<<<<<<<<<<<<" >> "$error_skip_template_file"
    else
        echo "skip_$package_group=$package_skip" >> "$error_skip_template_file"
    fi
    if [ -z "$package_level" ]
    then
        failure=true
        line_error=true
        $line_error || error_lines="$error_lines$line\n"
        error_level="$error_level$package_group\n"
    fi

    if [ -z "$package_type" ]
    then
        failure=true
        line_error=true
        $line_error || error_lines="$error_lines$line\n"
        error_type="$error_type$package_group\n"
    fi

    if [ -z "$package_group" ]
    then
        failure=true
        line_error=true
        $line_error || error_lines="$error_lines$line\n"
        error_group="$error_group$error_lines\n"
    fi

    if [ -z "$package_target_dir" ]
    then
        failure=true
        line_error=true
        $line_error || error_lines="$error_lines$line\n"
        error_target_dir="$error_target_dir$package_group\n"
    fi

    if [ -z "$package_build" ]
    then
        failure=true
        line_error=true
        $line_error || error_lines="$error_lines$line\n"
        error_build="$error_build$package_group\n"
    fi
done <<< "$(packages)"
################################################################################
# report errors
################################################################################

if $failure
then
    echo -e '\033[0;31m'
    echo -e "ERROR: Your config is broken (profile: $loaded_profile)"

fi

if ! [ -z "$error_flags" ]
then
    echo
    echo -e "These flags were not set:"
    echo -e "$error_flags" | sort -u | xargs -I% echo "    %"
    echo
fi

if ! [ -z "$error_lines" ]
then
    echo
    echo -e "These lines are broken in the current config:\n$error_lines"
    echo
fi

if ! [ -z "$error_skip" ]
then
    cat "$error_skip_template_file" | tr " " "~" | tr "=" " "           \
                                    | sort -u -b --ignore-case -k 1,1   \
                                    | tr " " "=" | tr "~" " "           \
                                    > "$error_skip_template_file_sorted"
    echo
    echo -e "Some groups were missing a configuration whether they sould be skipped!"
    echo -e "Set the variable skip_<groupname> either to true or to false"
    echo -e "This are the groups:"
    echo -e "$error_skip" | sort -u | xargs -I% echo "    %"
    echo -e "There is a template in '$error_skip_template_file_sorted'"
    echo
fi

if ! [ -z "$error_level" ]
then
    echo
    echo -e "Some groups were missing a level or have an invalid level!"
    echo -e "This is an error in zzz_packages"
    echo -e "This are the groups:"
    echo -e "$error_level" | sort -u | xargs -I% echo "    %"
    echo
fi

if ! [ -z "$error_group" ]
then
    echo
    echo -e "Some groups were not set!"
    echo -e "This is an error in zzz_packages"
    echo -e "This are the lines:"
    echo -e "$error_skip" | sort -u | xargs -I% echo "    %"
    echo
fi

if ! [ -z "$error_type" ]
then
    echo
    echo -e "Some groups are missing a type!"
    echo -e "This is an error in zzz_packages"
    echo -e "This are the groups:"
    echo -e "$error_type" | sort -u | xargs -I% echo "    %"
    echo
fi

if ! [ -z "$error_target_dir" ]
then
    echo
    echo -e "Some groups are missing a target dir!"
    echo -e "This is an error in zzz_packages"
    echo -e "This are the groups:"
    echo -e "$error_target_dir" | sort -u | xargs -I% echo "    %"
    echo
fi

if ! [ -z "$error_build" ]
then
    echo
    echo -e "Some groups are missing a build config!"
    echo -e "This is an error in zzz_packages"
    echo -e "This are the groups:"
    echo -e "$error_build" | sort -u | xargs -I% echo "    %"
    echo
fi

if $failure
then
    echo -e "ERROR: Your config is broken (profile: $loaded_profile)"
    echo -e '\033[0m'
    echo "something in your config is wrong!"
    exit 1
fi
set -x

bluecho "Your config looks ok (profile: $loaded_profile)"

